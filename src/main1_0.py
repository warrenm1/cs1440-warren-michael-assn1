#!/usr/bin/env python3

from Report import Report
rpt = Report()

import sys

def unique(l):
    d = dict()
    for i in l:
        if i not in d:
            d[i] = 1
        else:
            d[i] = 1

    count = 0
    for key in d:
        if d[key] == 1:
            count += 1
    return count;
def cache_co(l):
    count = 0
    for i in l:
        count += 1
        if i[2] == '49005':
            return count;

#initializing report variables
datadir = sys.argv[1]

##TODO: change code to use dictionaries and sets
soft_estab_top = []
soft_empl_top = []
soft_wage_top = []
soft_estab = []
soft_empl = []
soft_wage = []
all_estab_top = []
all_empl_top = []
all_wage_top = []
all_estab = []
all_empl = []
all_wage = []

#Gather information from csv
with open(datadir + '/2017.annual.singlefile.csv') as file:
    for line in file:
        line = line.rstrip()
        line = line.replace('"','')
        fields = line.split(',')

        #Software
        if fields[2] == '5112' and fields[1] == '5':
            if fields[0].startswith('U') or fields[0].startswith('C'):
               continue
            else:
                if fields[0].endswith('000'):
                    continue
                else:
                    rpt.soft.count += 1
                    rpt.soft.total_estab += int(fields[8])
                    rpt.soft.total_empl += int(fields[9])
                    rpt.soft.total_pay += int(fields[10])

                    soft_estab_top.append((fields[0],fields[8]))
                    soft_estab_top.sort()
                    soft_empl_top.append((fields[0],fields[9]))
                    soft_empl_top.sort()
                    soft_wage_top.append((fields[10],fields[0]))
                    soft_wage_top.sort()

                    #Distinct and Unique setup
                    soft_estab.append(fields[8])
                    soft_empl.append(fields[9])
                    soft_wage.append(fields[10])


        #Industry
        elif fields[2] == '10' and fields[1] == '0':
            if fields[0].startswith('U') or fields[0].startswith('C'):
                pass
            else:
                if fields[0].endswith('000'):
                    pass
                else:
                    rpt.all.count += 1
                    rpt.all.total_estab += int(fields[8])
                    rpt.all.total_empl += int(fields[9])
                    rpt.all.total_pay += int(fields[10])
                    all_estab_top.append((fields[0], fields[8]))
                    all_estab_top.sort()
                    all_empl_top.append((fields[0], fields[9]))
                    all_empl_top.sort()
                    all_wage_top.append((fields[0], fields[10]))
                    all_wage_top.sort()

                    all_estab.append(fields[8])
                    all_empl.append(fields[9])
                    all_wage.append(fields[10])

all_estab_top.reverse()
all_empl_top.reverse()
all_wage_top.reverse()
soft_wage_top.reverse()
soft_empl_top.reverse()
soft_estab_top.reverse()

#unique and distinct
s = set(soft_wage)
rpt.soft.distinct_pay = len(s)
rpt.soft.unique_pay = unique(soft_wage)
s = set(soft_estab)
rpt.soft.distinct_estab = len(s)
rpt.soft.unique_estab = unique(soft_estab)
s = set(soft_empl)
rpt.soft.distinct_empl = len(s)
rpt.soft.unique_empl = unique(soft_empl)

s = set(all_wage)
rpt.all.distinct_pay = len(s)
rpt.all.unique_pay = unique(all_wage)
s = set(all_estab)
rpt.all.distinct_estab = len(s)
rpt.all.unique_estab = unique(all_estab)
s = set(all_empl)
rpt.all.distinct_empl = len(s)
rpt.all.unique_empl = unique(all_empl)
del s

##TODO: per capita implementation here?

with open(datadir + '/area_titles.csv') as fips:
    for lines in fips:
        lines = lines.rstrip()
        lines = lines.replace('"', '')
        field = lines.split(',', 1)

        ses,sem,sw,aes,aem,aw = 5,5,5,5,5,5
        for top5 in soft_estab_top:
            if ses == 0:
                break
            if field[0] == top5[0]:
                rpt.soft.top_annual_estab.append(tuple((field[1],top5[1])))
            ses -= 1
        for top5 in soft_empl_top:
            if sem == 0:
                break
            if field[0] == top5[0]:
                rpt.soft.top_annual_avg_emplvl.append(tuple((field[1],top5[1])))
            sem -= 1

        for top5 in soft_wage_top:
            if sw == 0:
                break
            if field[0] == top5[1]:
                rpt.soft.top_annual_wages.append(tuple((field[1],top5[0])))
            sw -= 1

        for top5 in all_estab_top:
            if aes == 0:
                break
            if field[0] == top5[0]:
                rpt.all.top_annual_estab.append(tuple((field[1],top5[1])))
            aes -= 1
        for top5 in all_empl_top:
            if aem == 0:
                break
            if field[0] == top5[0]:
                rpt.all.top_annual_avg_emplvl.append(tuple((field[1],top5[1])))
            aem -= 1
        for top5 in all_wage_top:
            if aw == 0:
                break
            if field[0] == top5[0]:
                rpt.all.top_annual_wages.append(tuple((field[1],top5[1])))
            aw -= 1

#Cache County Placement
rpt.all.cache_co_pay_rank = cache_co(all_wage_top)
rpt.all.cache_co_estab_rank = cache_co(all_estab_top)
rpt.all.cache_co_empl_rank = cache_co(all_empl_top)
rpt.soft.cache_co_pay_rank = cache_co(soft_wage_top)
rpt.soft.cache_co_estab_rank = cache_co(soft_estab_top)
rpt.soft.cache_co_empl_rank = cache_co(soft_empl_top)

print(rpt)
#!/usr/bin/env python3

from Report import Report
rpt = Report()
import sys
import operator

#Unique Function
def unique(l):
    d = dict()
    for k in l:
        if k not in d:
            d[k] = 1
        else:
            d[k] += 1

    count = 0
    for key in d:
        if d[key] == 1:
            count += 1

    return count

#Distinct Function
def distinct(l):
    d = dict()
    for i in l:
        if i not in d:
            d[i] = 1
        else:
            d[i] += 1

    return len(d)

#Cache Co Placement Function
def cache_co(l):
    count = 0
    for c in l:
        count += 1
        if c[1] == '49005':
            return count

#initializing report variables
#[(numerical value, 'area_fips')]
soft_estab = []
soft_empl = []
soft_pay = []

all_estab = []
all_empl = []
all_pay = []

#['numerical value']
ses = []
sem = []
sp = []

aes = []
aem = []
ap = []

#integer value
s_es_tot = 0
s_em_tot = 0
s_p_tot = 0

a_es_tot = 0
a_em_tot = 0
a_p_tot = 0

soft_count = 0
all_count = 0

dummy = 0
datadir = sys.argv[1]

#Reading from FIPS File
with open(datadir + '/2017.annual.singlefile.csv') as file:
    for line in file:
        line = line.rstrip()
        line = line.replace('"','')
        fields = line.split(',')

        #Software
        if fields[2] == '5112' and fields[1] == '5':
            if fields[0].startswith('U') or fields[0].startswith('C') or fields[0].endswith('000'):
                dummy += 0
            else:
                soft_count +=1

                soft_pay.append((int(fields[10]),fields[0]))
                soft_estab.append((int(fields[8]),fields[0]))
                soft_empl.append((int(fields[9]),fields[0]))

                s_p_tot += int(fields[10])
                s_es_tot += int(fields[8])
                s_em_tot += int(fields[9])

                sp = fields[10]
                ses = fields[8]
                sem = fields[9]

        #Industry
        elif fields[2] == '10' and fields[1] == '0':
            if fields[0].startswith('U') or fields[0].startswith('C') or fields[0].endswith('000'):
                dummy += 0
            else:
                all_count += 1

                all_pay.append((int(fields[10]),fields[0]))
                all_estab.append((int(fields[8]),fields[0]))
                all_empl.append((int(fields[9]),fields[0]))

                a_p_tot += int(fields[10])
                a_es_tot += int(fields[8])
                a_em_tot += int(fields[9])

                ap = fields[10]
                aes = fields[8]
                aem = fields[9]

#Count
rpt.soft.count = soft_count
rpt.all.count = all_count

#Totals
rpt.soft.total_pay = s_p_tot
rpt.soft.total_estab = s_es_tot
rpt.soft.total_empl = s_em_tot

rpt.all.total_pay = a_p_tot
rpt.all.total_estab = a_es_tot
rpt.all.total_empl = a_em_tot

#Sorting the Tops
soft_pay.sort(reverse=True)
soft_estab.sort(reverse=True)
soft_empl.sort(reverse=True)

all_pay.sort(reverse=True)
all_estab.sort(reverse=True)
all_empl.sort(reverse=True)

#Reading Area Titles
with open(datadir + '/area_titles.csv') as fips:
    for lines in fips:
        lines = lines.rstrip()
        lines = lines.replace('"','')
        field = lines.split(',',1)

        i = 0
        #if this doesn't work...change name in list and add the list
        while i < 5:
            if field[0] == soft_estab[i][1]:
                rpt.soft.top_annual_estab.append((field[1],soft_estab[i][0]))
            if field[0] == soft_empl[i][1]:
                rpt.soft.top_annual_avg_emplvl.append((field[1],soft_empl[i][0]))
            if field[0] == soft_pay[i][1]:
                rpt.soft.top_annual_wages.append((field[1],soft_pay[i][0]))

            if field[0] == all_estab[i][1]:
                rpt.all.top_annual_estab.append((field[1],all_estab[i][0]))
            if field[0] == all_empl[i][1]:
                rpt.all.top_annual_avg_emplvl.append((field[1],all_empl[i][0]))
            if field[0] == all_pay[i][1]:
                rpt.all.top_annual_wages.append((field[1],all_pay[i][0]))
            i += 1

rpt.soft.top_annual_estab.sort(key=operator.itemgetter(1),reverse=True)
rpt.soft.top_annual_avg_emplvl.sort(key=operator.itemgetter(1),reverse=True)
rpt.soft.top_annual_wages.sort(key=operator.itemgetter(1),reverse=True)

rpt.all.top_annual_estab.sort(key=operator.itemgetter(1),reverse=True)
rpt.all.top_annual_avg_emplvl.sort(key=operator.itemgetter(1),reverse=True)
rpt.all.top_annual_wages.sort(key=operator.itemgetter(1),reverse=True)


#Per Capita Logic
rpt.soft.per_capita_avg_wage = rpt.soft.total_pay / rpt.soft.total_empl
rpt.all.per_capita_avg_wage = rpt.all.total_pay / rpt.all.total_empl

#Unique and Distinct Logic
setsp = []
for i in soft_pay:
    setsp.append(i[0])
rpt.soft.distinct_pay = distinct(setsp)
rpt.soft.unique_pay = unique(setsp)

setses = []
for i in soft_estab:
    setses.append(i[0])
rpt.soft.distinct_estab = distinct(setses)
rpt.soft.unique_estab = unique(setses)

setsem = []
for i in soft_empl:
    setsem.append(i[0])
rpt.soft.distinct_empl = distinct(setsem)
rpt.soft.unique_empl = unique(setsem)

setap = []
for i in all_pay:
    setap.append(i[0])
rpt.all.distinct_pay = distinct(setap)
rpt.all.unique_pay = unique(setap)

setaes = []
for i in all_estab:
    setaes.append(i[0])
rpt.all.distinct_estab = distinct(setaes)
rpt.all.unique_estab = unique(setaes)

setaem = []
for i in all_empl:
    setaem.append(i[0])
rpt.all.distinct_empl = distinct(setaem)
rpt.all.unique_empl = unique(setaem)

#Cache Co Rank Logic
rpt.soft.cache_co_pay_rank = cache_co(soft_pay)
rpt.soft.cache_co_estab_rank = cache_co(soft_estab)
rpt.soft.cache_co_empl_rank = cache_co(soft_empl)
rpt.all.cache_co_pay_rank = cache_co(all_pay)
rpt.all.cache_co_estab_rank = cache_co(all_estab)
rpt.all.cache_co_empl_rank = cache_co(all_empl)

print(rpt)
# Running your program

Launch your program from this directory.  It must be written so that it is
invoked like this:

    python src/main.py data

The first argument will be the name of a directory containing the input data.
Your code must allow for this directory to be specified by the user.  The names
of the files within that directory may be hardcoded.



##Note to Graders
This program is working wonderfully with the command line requirements.

Let me know if there are any questions or conflicts that need to be addressed.

Thanks for all of your hard work.


##Citing Ideas
A good chunk from Erik Falor from class

minor bug fixing with tuple assigning through stackoverflow

the rest was me hashing it out and sleeping on ideas

